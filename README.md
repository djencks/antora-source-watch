# Antora Source-Watch Extension
:version: 0.0.5-rc.2

This is an Antora extension that restricts what is loaded into the content catalog and watches configured sources to rerun the build on changes.

See the more complete README.adoc at gitlab.
