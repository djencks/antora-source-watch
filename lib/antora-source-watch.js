'use strict'

const userRequire = require('@antora/user-require-helper')
const expandPath = require('@antora/expand-path-helper')
const bs = require('browser-sync').create()
const picomatch = require('picomatch')
const ospath = require('path')
const chokidar = require('chokidar')
const { posix: path } = ospath

const GIT_URL_DETECTOR_RX = /:(?:\/\/|[^/\\])/

const Lock = require('./lock.js')
const processorLock = new Lock()

const generator = '@antora/site-generator'
let logger
let isGenerator

module.exports = async function (playbook) {
  isGenerator = true
  const dot = playbook.dir
  const userRequireContext = { dot, paths: [dot, __dirname] }
  let generateSite
  try {
    generateSite =
      (generateSite = userRequire(generator, userRequireContext)).length === 1
        ? generateSite.bind(null, playbook)
        : generateSite.bind(null, [playbook], process.env)//not gonna work
  } catch (err) {
    let msg = '(default) Generator not found or failed to load.'
    if (generator && generator.charAt() !== '.') msg += ` Try installing the '${generator}' package.`
    throw new Error(msg)
  }

  const generate = async function (_) {
    try {
      const hasQueuedEvents = await processorLock.acquire()
      if (!hasQueuedEvents) {
        logger && logger.info('starting build')
        await generateSite()
        logger && logger.info('build complete')
      }
    } catch (err) {
      logger ? logger.error(err) : console.log('error', err)
    } finally {
      processorLock.release()
    }
  }
  await generate(null)

  const ignored = watch.watchExcludes
  ignored.push(/(^|[/\\])\../)
  const paths = watch.watchIncludes.length ? watch.watchIncludes : ['*']
  logger && logger.debug('watching', paths)
  const watcher = chokidar.watch(paths, {
    ignored,
    persistent: true,
    awaitWriteFinish: {
      stabilityThreshold: 100,
      pollInterval: 50,
    },
  })

  watcher.on('change', async (e) => await generate(e))
  // watcher.on('add', async (e) => await generate(e))
  watcher.on('unlink', async (e) => await generate(e))
  return new Promise((resolve, reject) => {})
}

function matcher (include, exclude) {
  return include ? picomatcher(include, exclude) : () => true
}

function picomatcher (include, exclude) {
  const options = exclude ? { ignore: exclude.split(',').map((term) => term.trim()) } : {}
  return picomatch(include.split(',').map((term) => term.trim()), options)
}

function addPaths (paths, base, partials) {
  partials && partials.split(',')
    .map((partial) => partial.trim())
    .map((partial) => paths.push(path.join(base, partial)))
}

/* from antora */
function getNavInfo (filepath, nav) {
  const index = nav.findIndex((candidate) => candidate === filepath)
  if (~index) return { index }
}

let serverStarted

let watch

module.exports.register = function ({ config, playbook }) {
  logger = this.getLogger('@djencks/antora-source-watch')
  config.logLevel && (logger.level = config.logLevel)
  const trace = logger.isLevelEnabled('trace')
  const debug = logger.isLevelEnabled('debug')

  const sourceMatchers = (config.sources || []).reduce((accum,
    { url, startPathIncludes, startPathExcludes, branchIncludes, branchExcludes }) => {
    accum[url] = {
      startPathMatcher: matcher(startPathIncludes, startPathExcludes),
      branchMatcher: matcher(branchIncludes, branchExcludes),
    }
    return accum
  }, {})
  logger.debug({ msg: 'sourceMatchers', sourceMatchers })
  watch = (config.sources || []).reduce((accum, { url, startPathIncludes, startPathExcludes }) => {
    if (!GIT_URL_DETECTOR_RX.test(url)) {
      url = expandPath(url, { dot: playbook.dir || '.' })
      addPaths(accum.watchIncludes, url, startPathIncludes || ' ')
      addPaths(accum.watchExcludes, url, startPathExcludes)
    }
    return accum
  }, { watchIncludes: [], watchExcludes: [] })
  logger.debug({ msg: 'watching', watch })

  const componentMatchers = (config.components || []).reduce((accum,
    { name, nameExcludes, version, versionExcludes, module, moduleExcludes, relativeIncludes, relativeExcludes }) => {
    accum.push({
      nameMatcher: matcher(name, nameExcludes),
      versionMatcher: matcher(version, versionExcludes),
      moduleMatcher: matcher(module, moduleExcludes),
      relMatcher: matcher(relativeIncludes, relativeExcludes),
    })
    return accum
  }, [])

  this.on('playbookBuilt',
    ({ playbook }) => {
      const sources = playbook.content.sources
        .filter((source) => sourceMatchers[source.url])
        .reduce((accum, source) => {
          logger.debug({ msg: 'considering source', source })
          const sourceMatcher = sourceMatchers[source.url]

          let startPaths = source.startPaths || (source.startPath ? [source.startPath] : null)
          if (startPaths) {
            startPaths = startPaths.filter(sourceMatcher.startPathMatcher)
            if (!startPaths.length) return accum //skip, no matching source_path
          }

          const branches = (Array.isArray(source.branches) ? source.branches : [source.branches])
            .filter(sourceMatcher.branchMatcher)
          if (!branches.length) return accum

          accum.push(Object.assign({}, source, { branches }, startPaths ? { startPaths } : {}))
          return accum
        }, [])
      this.updateVariables({
        playbook: Object.assign({}, playbook,
          { content: Object.assign({}, playbook.content, { sources }) }),
      })
      logger.debug({ msg: 'updated playbook with filtered sources', sources })
    }
  )

  const PATH_RX = /modules\/(?<module>[^/]*)\/(?<family>[^/]*)\/(?<relative>.*)/
  this.on('contentAggregated',
    ({ contentAggregate }) => {
      if (!componentMatchers.length) {
        logger.info('no component content filtering')
        return
      }
      logger.info('adjusting contentAggregate')
      contentAggregate.splice(0, contentAggregate.length,
        ...contentAggregate.filter((componentVersionData) =>
          componentMatchers.some(({ nameMatcher, versionMatcher, moduleMatcher, relMatcher }) => {
            const { name, version, nav } = componentVersionData
            if (nameMatcher(name) && versionMatcher(version)) {
              componentVersionData.files = componentVersionData.files.filter((file) => {
                try {
                  const filepath = file.path
                  if (nav && getNavInfo(filepath, nav)) return true
                  const pathMatch = filepath.match(PATH_RX)
                  trace && pathMatch && logger.trace({ pathMatch: pathMatch.groups })
                  return !pathMatch ||
                    (pathMatch.groups.family !== 'pages' ||
                      (moduleMatcher(pathMatch.groups.module) &&
                        relMatcher(pathMatch.groups.relative)))
                  //always include component start page?
                } catch (e) {
                  logger.error(e)
                  return false
                }
              })

              return componentVersionData.files.length
            }
          }
          )))
      if (debug) {
        logger.debug({ msg: 'filtered contentAggregate', contentAggregateLength: contentAggregate.length })
        contentAggregate.forEach(({ name, version, files }) => logger.debug(`component: ${name}, version: ${version}, files: ${files.length}`))
      }
    }
  )

  this.on('sitePublished', ({ playbook }) => {
    if (isGenerator) {
      if (serverStarted) {
        logger.info('reloading')
        bs.reload()
      } else {
        logger.info('starting server')
        bs.init({
          server: (playbook.output && playbook.output.dir) || 'build/site',
        })
        serverStarted = true
      }
    }
  })
}
