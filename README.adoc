= Antora {extension} Extension
:extension: source-watch
:full-extension-name: @djencks/antora-{extension}
:extension-version: 0.0.5-rc.2
:source-repository: https://gitlab.com/{full-extension-name}
:description: This pipeline extension selects a portion of the sources and aggregate to build and watches the selected sources for changes.
:antora-version: 3.0.0-rc.2
:site-manifest-version: 0.0.3-rc.2

== Description

{description}
It wraps the default site generator to loop, rebuilding the (partial) selected sources on detected changes.
It is best used in combination with the `@djencks/antora-site-manifest` extension to provide a full site.
Use two build stages, the first of the whole site to generate a full site manifest, and the second with this `antora-{extension}` in collaboration with the site-manifest extension to rebuild a selection of sources.

After an initial build, a watcher is started on the configured source/start_paths, and `browser-sync` is used to serve the site and update browsers on changes.

It can also be used as an Antora extension alone, in which case it will perform a single partial build without starting a server or watching files.

The code is located at link:{source-repository}[]

== Installation

To use in your documentation project, the simplest way is to have a package.json like this:

[source,json,subs="+attributes"]
----
{
  "description": "---",
  "scripts": {
    "plain-install": "npm i --cache=.cache/npm --no-optional",
    "build-full": "antora antora-playbook-full.yml --stacktrace --fetch",
    "build-partial": "antora antora-playbook-partial.yml --stacktrace"
  },
  "devDependencies": {
    "@antora/cli": "^{antora-version}",
    "@antora/site-generator": "^{antora-version}",
    "{full-extension-name}": "^{extension-version}",
    "@djencks/antora-site-manifest": "^{site-manifest-version}"
  }
}
----

You will need two playbooks:

.antora-playbook-full.yml, to generate the full site manifest
[source,yml,subs="+attributes"]
----
antora:
  extensions:
    - require: '@djencks/antora-site-manifest'
      export-site-manifest: true
      export-site-navigation-data: true
----

.antora-playbook-partial.yml, to rebuild portions of the site, importing the full site manifest
[source,yml,subs="+attributes"]
----
antora:
  extensions:
    - require: '{full-extension-name}'
      sources:
        - url: .
          start-path-includes: modules/datascience,modules/4.0-best-practices


    - require: '@djencks/antora-site-manifest'
      import-manifests:
        - primary-site-manifest-url: ./public/site-manifest.json # <1>
      partial-components: true # <2>
      local-urls: true # <3>
----
<1> Location of full site-manifest from the stage 1 build.
<2> Required configuration parameter, so that all pages not built will be included in the site.
<3> The site-manifest will contain the URLs for the deployed site.
To get a locally viewable site, set the `local-urls` flag to true so xrefs to pages supplied using the site-manifest will be to the local copy.

== Configuration

Two levels of filtering are supported.

=== `content.sources` filtering

This more or less matches `content.sources`.

[source,yml]
----
antora:
  extensions:
    - require: '{full-extension-name}'
      sources:
        - url: . #<1>
          branch_includes: m* #<2>
          branch_excludes: main #<3>
          start_path_includes: modules/datascience,modules/4.0-* #<4>
          start_path_excludes: modules/4.0-best-practices #<5>
----
<1> A url matching a url in the `contents.sources` key.
<2> A comma-separated list of picomatch compatible matches for branches.
<3> A comma-separated list of picomatch compatible branch matches for the `ignore` option.
<4> A comma-separated list of picomatch compatible matches for start_paths.
<5> A comma-separated list of picomatch compatible start_path matches for the `ignore` option.

Only sources matching these criteria are initially loaded into the content aggregate.

This source filtering information is combined to configure the chokidar watcher.

WARNING: Single branches and lists of branches in the playbook are supported.
Comma-separated lists are not supported.
There is no attempt to expand branch or start path patterns: matching is conducted on the strings found in the playbook.

=== Aggregate filtering

The content aggregate may be additionally filtered by component, version, module, and relative.
Only pages are filtered: everything else is included.
All keys are optional but at least one positive match must be supplied.
All filter keys use picomatch with comma separated lists of includes and excludes.
The excludes, if present, are supplied to the picomatch 'ignore' option.
An exclude without a corresponding include is ignored.

Note that multiple match sets may be supplied under the components key.

[source,yml]
----
pipeline:
  extensions:
    - require: '{full-extension-name}'
      components:
        - name: f*
          name_excludes: foo
          version: '1.*'
          version_excludes: '1.1'
          module: b*
          module_excludes: baz
          relative_includes: **/foo*.adoc
          relative_excludes: **/foo-bar.adoc
----

== Sample project

The repository `https://github.com/djencks/training-v3.git` branch `antora-source-watch` adapts the `https://github.com/neo4j-contrib/training-v3.git` project to use this extension.
