= how to release to npm
:version: 0.0.5-rc.2
:repo: antora-source-watch
:bundle: @djencks-{repo}
:url: https://gitlab.com/djencks/{repo}/-/jobs/artifacts/main/raw/{bundle}-{version}.tgz?job=bundle-stable

* update this file, package.json, and the two README files with the new version.
* push to main at gitlab
* When the CI completes the bundle to release should be at:
{url}

* run
npm publish --access public {url} --dry-run

* run
npm publish --access public {url}



