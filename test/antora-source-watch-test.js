/* eslint-env mocha */
'use strict'

const { expect } = require('chai')
const EventEmitter = require('events')
const antoraSourceWatch = require('./../lib/antora-source-watch')

describe('antora-source-watch tests', () => {
  let eventEmitter
  let vars
  let log

  beforeEach(() => {
    eventEmitter = new EventEmitter()
    log = []
    antoraSourceWatch.on = (event, code) => {
      eventEmitter.on(event, code)
    }
    antoraSourceWatch.updateVariables = (vars_) => {
      vars = vars_
    }
    antoraSourceWatch.getLogger = (name) => {
      return {
        trace: (msg) => log.push({ level: 'trace', msg }),
        debug: (msg) => log.push({ level: 'debug', msg }),
        info: (msg) => log.push({ level: 'info', msg }),
        warn: (msg) => log.push({ level: 'warn', msg }),
        error: (msg) => log.push({ level: 'error', msg }),
        isLevelEnabled: (level) => true,
      }
    }
  })

  describe('source filter tests', () => {
    it('sources filter positive', () => {
      const playbook = { content: { sources: [{ url: 'foo', branches: ['main'] }] } }
      const config = { sources: [{ url: 'foo' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('playbookBuilt', { playbook })
      expect(vars.playbook.content.sources.length).to.equal(1)
    })

    it('sources filter negative', () => {
      const playbook = { content: { sources: [{ url: 'foo', branches: ['main'] }] } }
      const config = { sources: [{ url: 'bar' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('playbookBuilt', { playbook })
      expect(vars.playbook.content.sources.length).to.equal(0)
    })

    it('startPath filter positive', () => {
      const playbook = { content: { sources: [{ url: 'foo', branches: ['main'], startPaths: ['sp1', 'sp2'] }] } }
      const config = { sources: [{ url: 'foo', startPathIncludes: 's*' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('playbookBuilt', { playbook })
      expect(vars.playbook.content.sources.length).to.equal(1)
      expect(vars.playbook.content.sources[0].startPaths.length).to.equal(2)
    })

    it('startPath filter selective', () => {
      const playbook = { content: { sources: [{ url: 'foo', branches: ['main'], startPaths: ['sp1', 'sp2'] }] } }
      const config = { sources: [{ url: 'foo', startPathIncludes: 's*1' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('playbookBuilt', { playbook })
      expect(vars.playbook.content.sources.length).to.equal(1)
      expect(vars.playbook.content.sources[0].startPaths.length).to.equal(1)
    })

    it('startPath filter negative', () => {
      const playbook = { content: { sources: [{ url: 'foo', branches: ['main'], startPaths: ['sp1'] }] } }
      const config = { sources: [{ url: 'foo', startPathIncludes: 't*' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('playbookBuilt', { playbook })
      expect(vars.playbook.content.sources.length).to.equal(0)
    })

    it('startPath filter negative exclude', () => {
      const playbook = { content: { sources: [{ url: 'foo', branches: ['main'], startPaths: ['sp1', 'sp2'] }] } }
      const config = { sources: [{ url: 'foo', startPathIncludes: 's*', startPathExcludes: 'sp1' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('playbookBuilt', { playbook })
      expect(vars.playbook.content.sources.length).to.equal(1)
      expect(vars.playbook.content.sources[0].startPaths.length).to.equal(1)
    })

    it('branch filter positive', () => {
      const playbook = { content: { sources: [{ url: 'foo', branches: ['main', 'many'], startPaths: ['sp1'] }] } }
      const config = { sources: [{ url: 'foo', branchIncludes: 'm*' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('playbookBuilt', { playbook })
      expect(vars.playbook.content.sources.length).to.equal(1)
      expect(vars.playbook.content.sources[0].branches.length).to.equal(2)
    })

    it('single branch filter positive', () => {
      const playbook = { content: { sources: [{ url: 'foo', branches: 'main', startPaths: ['sp1'] }] } }
      const config = { sources: [{ url: 'foo', branchIncludes: 'm*' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('playbookBuilt', { playbook })
      expect(vars.playbook.content.sources.length).to.equal(1)
      expect(vars.playbook.content.sources[0].branches.length).to.equal(1)
    })

    it('branch filter selective', () => {
      const playbook = { content: { sources: [{ url: 'foo', branches: ['main', 'x'], startPaths: ['sp1'] }] } }
      const config = { sources: [{ url: 'foo', branchIncludes: 'm*' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('playbookBuilt', { playbook })
      expect(vars.playbook.content.sources.length).to.equal(1)
      expect(vars.playbook.content.sources[0].branches.length).to.equal(1)
    })

    it('branch filter negative', () => {
      const playbook = { content: { sources: [{ url: 'foo', branches: ['main'], startPaths: ['sp1'] }] } }
      const config = { sources: [{ url: 'foo', branchIncludes: 't*' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('playbookBuilt', { playbook })
      expect(vars.playbook.content.sources.length).to.equal(0)
    })

    it('branch filter negative exclude', () => {
      const playbook = { content: { sources: [{ url: 'foo', branches: ['main', 'many'], startPaths: ['sp1'] }] } }
      const config = { sources: [{ url: 'foo', branchIncludes: 'm*', branchExcludes: 'main' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('playbookBuilt', { playbook })
      expect(vars.playbook.content.sources.length).to.equal(1)
      expect(vars.playbook.content.sources[0].branches.length).to.equal(1)
    })
  })

  describe('aggregate filter tests', () => {
    const playbook = {}

    it('allows 1 files', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = {}
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(1)
    })

    it('With no conditions, does not filter out no files', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [] },
      ]
      const config = {}
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(1)
    })

    it('Component filter positive', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
        { name: 'fee', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = { components: [{ name: 'foo' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(1)
    })

    it('Component filter pattern positive', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
        { name: 'fee', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = { components: [{ name: 'f*' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(2)
    })

    it('Component filter pattern exclusion', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
        { name: 'fee', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = { components: [{ name: 'f*', nameExcludes: 'fo*' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(1)
    })

    it('Multiple component filters positive', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
        { name: 'fee', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = { components: [{ name: 'foo' }, { name: 'fee' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(2)
    })

    it('component filter negative', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = { components: [{ name: 'bar' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(0)
    })

    it('version filter positive', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = { components: [{ version: '1.0' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(1)
    })

    it('version filter negative', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = { components: [{ version: 'bar' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(0)
    })

    it('module filter positive', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/m1/pages/foo.adoc' }] },
        { name: 'foo', version: '1.0', files: [{ path: 'modules/m2/pages/foo.adoc' }] },
      ]
      const config = { components: [{ module: 'm1' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(1)
    })

    it('module filter pattern positive', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/m1/pages/foo.adoc' }] },
        { name: 'foo', version: '1.0', files: [{ path: 'modules/m2/pages/foo.adoc' }] },
      ]
      const config = { components: [{ module: 'm*' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(2)
    })

    it('module filter exclusion', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/m1/pages/foo.adoc' }] },
        { name: 'foo', version: '1.0', files: [{ path: 'modules/m2/pages/foo.adoc' }] },
      ]
      const config = { components: [{ module: 'm*', moduleExcludes: 'm1' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(1)
    })

    it('module filter negative', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = { components: [{ module: 'NOT-ROOT' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(0)
    })

    it('non-page positive', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/images/foo.svg' }] },
      ]
      const config = {}
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(1)
    })

    it('relative filter positive', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = { components: [{ relativeIncludes: 'foo.*' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(1)
    })

    it('relative filter negative', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = { components: [{ relativeIncludes: 'bar.*' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(0)
    })

    it('relative filter excludes negative', () => {
      const contentAggregate = [
        { name: 'foo', version: '1.0', files: [{ path: 'modules/ROOT/pages/foo.adoc' }] },
      ]
      const config = { components: [{ relativeIncludes: 'foo.*', relativeExcludes: 'foo.adoc' }] }
      antoraSourceWatch.register({ config, playbook })
      eventEmitter.emit('contentAggregated', { contentAggregate })
      expect(contentAggregate.length).to.equal(0)
    })
  })
})
